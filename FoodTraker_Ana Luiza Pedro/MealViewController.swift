//
//  ViewController.swift
//  FoodTraker_Ana Luiza Pedro
//
//  Created by Station_04 on 18/11/19.
//  Copyright © 2019 Station_04. All rights reserved.
//

import UIKit
import os.log

class MealViewController: UIViewController ,UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
        //MARK: Properties
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var saveButton: UIBarButtonItem!
 
    
    /*
     This value is either passed by `MealTableViewController` in
     `prepare(for:sender:)`
     or constructed as part of adding a new meal.
     */
    
    var meal: Meal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Handle the text field's user input through delegates callbacks
        nameTextField.delegate = self
        
        // Set up views if editing an existing Meal.
        if let meal = meal {
        navigationItem.title = meal.name
        nameTextField.text = meal.name
        photoImageView.image = meal.photo
        ratingControl.rating = meal.rating
        
        }
            
        // Enable the Save button only if the text field has a valid Meal name.
       //updateSaveButtonState()
        
    }
    
        //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //Hide softKeyBoard
        textField.resignFirstResponder()
        return true
    }

        //MARK: Navigation
    
    // This method lets you configure a view controller before it's presented.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        super.prepare(for: segue, sender: sender)
        
        // Configure the destination view controller only when the save button is pressed.
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            os_log("The save button was not pressed, cancelling",log: OSLog.default, type: .debug)
            return
        }
        
        let name = nameTextField.text ?? ""
        let photo = photoImageView.image
        let rating = ratingControl.rating
       
        // Set the meal to be passed to MealTableViewController after the unwind segue.
        meal = Meal(name: name, photo: photo, rating: rating)
        
        
        
    }
    
    
        //MARK: Actions
    @IBAction func selectImagePhotoLibary(_ sender: UITapGestureRecognizer) {
        //Hide softKeyBoard
        nameTextField.resignFirstResponder()
        
        // UIImagePickerController is a view controller that lets a user pick media from their photo library.
        let ImagePickerController = UIImagePickerController()
       
        // Only allow photos to be picked, not taken.
        ImagePickerController.sourceType = .photoLibrary
        
        // Make sure ViewController is notified when the user picks an image.
        ImagePickerController.delegate = self
        present ( ImagePickerController , animated : true , completion : nil )

    }
    
        //MARK: UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel ( _ picker : UIImagePickerController ) {
        // Dismiss the picker if the user canceled.
        dismiss ( animated : true , completion : nil )
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        // The info dictionary may contain multiple representations of the image. You want to use the original].
        guard let selectImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            fatalError ( "Expected a dictionary containing an image, but was provided the following: \(info)")
        }

        // Set photoImageView to display the selected image.
        photoImageView.image = selectImage
        
        // Dismiss the picker.
        dismiss ( animated : true , completion : nil )
    }
    
    //MARK: Private Methods
    func updateSaveButtonState() {
        if nameTextField.text == "" {
            saveButton.isEnabled = true
        }
        else {
            saveButton.isEnabled = true
        }
   }
    
    
    @IBAction func cancel(_sender: UIBarButtonItem) {
        // Depending on style of presentation (modal or push presentation), this view controller needs to be dismissed in two different ways.
        let isPresentigInAddMealMode = presentingViewController is UINavigationController
        
        if isPresentigInAddMealMode{
            dismiss(animated: true, completion: nil)
        }
 
        else if let owningNavigationController = navigationController{
            owningNavigationController.popViewController(animated:true)
        }
        else{
            fatalError("The MealViewController is not inside a navigation controller.")
    
        }
    }
}
