//
//  MealTableViewCell.swift
//  FoodTraker_Ana Luiza Pedro
//
//  Created by Station_04 on 27/11/19.
//  Copyright © 2019 Station_04. All rights reserved.
//

import UIKit



class MealTableViewCell: UITableViewCell {

    //MARK: Propierts
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}






