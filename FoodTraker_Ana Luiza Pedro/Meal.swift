//
//  Meal.swift
//  FoodTraker_Ana Luiza Pedro
//
//  Created by Station_04 on 26/11/19.
//  Copyright © 2019 Station_04. All rights reserved.
//

import os.log

import UIKit

class Meal: NSObject, NSCoding {
    
    //MARK: Properties
    var name: String
    var photo: UIImage?
    var rating: Int
    
    
    //MARK: Types
    struct PropertyKey {
        static let name = "name"
        static let photo = "photo"
        static let rating = "rating"
        }
    
    //MARK: Initialization
    init?(name: String, photo: UIImage? , rating: Int ) {
    
        guard !name.isEmpty else{
            return nil
        }
        
        guard (rating >= 0) && (rating<=5) else {
            return nil 
        }
            
        // Initialaze stored properties
        self.name = name
        self.photo = photo
        self.rating = rating
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(photo, forKey: PropertyKey.photo)
        aCoder.encode(rating, forKey: PropertyKey.rating)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        // The name is required. If we cannot decode a name string, the initializer should fail.
        guard (aDecoder.decodeObject(forKey: PropertyKey.name) as? String) != nil
        else {
            os_log("Unable to decode the name for a Meal object.", log: OSLog.default, type: .debug)
            return nil
        }
        
            // Because photo is an optional property of Meal, just use conditional cast.
                let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as? UIImage
                let rating = aDecoder.decodeInteger(forKey: PropertyKey.rating)
        
        // Must call designated initializer.
        self.init(name: name, photo: photo, rating: rating)
        
    }
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("meals")
    
}










































