//
//  RatingControl.swift
//  FoodTraker_Ana Luiza Pedro
//
//  Created by Station_04 on 22/11/19.
//  Copyright © 2019 Station_04. All rights reserved.
//

import UIKit



@IBDesignable class RatingControl: UIStackView {
    
    //MARK Properties
    private var ratingButtons = [UIButton]()
    var rating = 0 {
        didSet{
         updateButtonSelectionState()
            
        }
    }
    
    @IBInspectable var starSize: CGSize = CGSize(width: 44.0, height: 44.0) {
        didSet {
            setupButtons()
            
        }
    }

    
    @IBInspectable var starCount: Int = 5 {
        didSet{
            setupButtons()
        }
        
    }
    
    
    //MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButtons()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        setupButtons()
    
    }
    
    //MARK: Button Action
    @objc func ratingButtonTapped(button: UIButton) {
        print("button pressed 👍")
    
        guard let index = ratingButtons.index(of: button) else {
            fatalError ( "The button, \( button ) , is not in the ratingButtons array: \( ratingButtons ) " )
        }
     
        // Calculate the rating of the selected button
        let selectedRating = index + 1
        
        if selectedRating == rating {
        // If the selected star represents the current rating, reset the rating to 0.
       rating = 0
        
        }
        else {
        // Otherwise set the rating to the selected star
        rating = selectedRating
        
        }
     print(rating)
    
    }
    
    
    
    //MARK: Private Methhods
    private func setupButtons() {
        
            // clear any existing buttons
        for button in ratingButtons {
        removeArrangedSubview(button)
            button.removeFromSuperview()
        }
        ratingButtons.removeAll()
        
        // Load Button Images
        let bundle = Bundle(for:type(of: self))
        let filledStar = UIImage(named: "filledStar", in: bundle, compatibleWith: self.traitCollection)
        let emptyStar = UIImage(named: "emptyStar", in: bundle, compatibleWith: self.traitCollection)
        let highlightedStar = UIImage(named: "highlightedStar", in: bundle, compatibleWith: self.traitCollection)
            
        
        for index in 0..<starCount {
            
            // Create the button
            let button = UIButton()
            button.backgroundColor = UIColor.red
            
         // Set the button images
            button.setImage(emptyStar, for: .normal)
            button.setImage(filledStar, for: .selected)
            button.setImage(highlightedStar, for: .highlighted)
            button.setImage(highlightedStar, for: [.highlighted, .selected])
            
            // Add contraints
            button.translatesAutoresizingMaskIntoConstraints = false
            button.heightAnchor.constraint(equalToConstant: 44.0).isActive = true
            button.widthAnchor.constraint(equalToConstant: 44.0).isActive = true
         
            // Set the accessibility label
            button.accessibilityLabel = "Set \(index + 1) star rating"
            
            // Setup the button action
            button.addTarget(self, action: #selector(RatingControl.ratingButtonTapped(button:)), for: .touchUpInside)
            
            // Add the button to the stack
            addArrangedSubview(button)
            
            // Add the new button to the rating button array
            ratingButtons.append(button)
    
        }
    updateButtonSelectionState()
        
    }

    private func updateButtonSelectionState()    {
        for (index, button) in ratingButtons.enumerated(){
            button.isSelected = index < rating
            
        }
    }
    
}
